# ESP32C3_Internet Clock
This project create a Clock use SNTP with ESP32-C3-01M-Kit and ESP-IDF tool
* Release Date: 2022-06-22
* Versions: WFCL v1.4

## Release Note
* 2022-06-22: Upload base project (WFCL v1.0)
* 2022-06-26: Update feature use WIFI QR Code to config STA for system (WFCL v1.1)
* 2022-07-01: Update feature keep tracking state of connection and change time format on OLED (WFCL v1.2)
* 2022-07-08: (WFCL v1.3)
    * Update Captive Portal help user can configure Wi-Fi, Timezone of the clock.
    * Add feature use button IO9 to display Wi-Fi QR Code while the clock is running.
* 2022-07-15: (WFCL v1.4)
    * Fix error of feature use button and change interface of Captive Portal
    * Add feature save and restore configuration parameters into NVS

## Feature
* Display Time use SNTP and the error in terms of time is negligible (reference to `time.is`) - Completely
* Display a WIFI QR Code help user can configure Wi-Fi configuration/location/format time - Progressing
    * Completely generate WIFI QR Code and display on OLED but now this project using OLED 0.96" two colors and string to generate QR is long so as to use QR version 3 so haven't display the complete QR -> Update: Solved problem QR by using an OLED 2.42" and one color (blue)
    * Completely creat a Webserver help user to configure some parameters for this project
    * Completely keep tracking state of Connection
    * Completely add some functions in Captive Portal help user can config UTC offset
    * Completely save and restore configuration parameters (SSID, Password, Timezone) into NVS


## How to use this project
* When power on the system, system will read from NVS flash some parameters. If NVS flash have them, system will use them to configure system without configuring from user.
* IF NVS flash is empty or that parameters are wrong, user need use smartphone to scan WiFi QR Code displayed on OLED. (*)
    * With iOS: User need open Settings -> Wi-Fi. Then OS will redirect to a Captive Portal that user can configure SSID and Password for connection and timezone of the system.
    * With Android: User need open Settings -> Wi-Fi -> Click on "ESP Clock" -> Click "Sign in". Then OS will redirect to a Captive Portal that user can configure SSID and Password for connection and timezone of the system.
* After configuring SSID and Password and saving it. The system will retry to connect to that AP.
    * Case 1: That AP parameters are right -> The system will display date, time and state of connection on OLED. (during 10 seconds)
    * Case 2: AP parameters are wrong or AP cannot connect to internet -> Please check your AP and internet of AP. After 10 seconds, the WiFi QR Code will blink and user need re-configure AP.
* While the clock is running, if user want to change one/more parameters, please press on button IO9 to display Wi-Fi QR Code and do (*) to set any parameter you want.


#### Build and Flash
* Run `idf.py build` to build project
* Run `idf.py -p PORT flash monitor` to flash project and open monitor on Console. If you have only one PORT in PC is connecting with only one kit, you can use `idf.py flash monitor`
