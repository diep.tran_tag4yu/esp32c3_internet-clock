#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "esp_netif.h"
#include "lwip/inet.h"
#include "esp_http_server.h"
#include "dns_server.h"
#include "nvs_flash.h"
#include "esp_sntp.h"
#include "driver/i2c.h"
#include <driver/gpio.h>

#include <u8g2.h>
#include "u8g2_esp32_hal.h"
#include "qrcodegen.h"

#define ESP_AP_SSID "TAG4YU CLOCK"
char ESP_AP_PASS[9] = "17020634";
#define ESP_AP_MAX_CONN 4
#define ESP_MAX_RETRY 10
#define DISCONNECT_SHOW_QR_FREQ 10
#define HEADER_QR "WIFI:T:WPA;S:"
#define MIDDLE_QR ";P:"
#define END_QR ";;"
#define divideNum 100000000
#define CONNECT_STR "Connecting to new Wi-Fi..."
#define DISCONNECT_STR "Cannot connect internet!"
#define NOTI1_STR "Please restart the clock"
#define NOTI2_STR "or re-setup..."
#define SCREEN_W 128
#define SCREEN_H 64
#define BTN_PIN 9

static const char *TAG = "SNTP_Clock";

u8g2_t u8g2;
char WIFI_QR_CODE[100] = "", STR_TIME_LOST[100] = "";
char country_firstDivision[200] = "Coordinated Universal Time";
extern const char root_start[] asm("_binary_root_html_start");
extern const char root_end[] asm("_binary_root_html_end");
extern const char save_start[] asm("_binary_save_html_start");
extern const char save_end[] asm("_binary_save_html_end");

static int s_retry_num = 0;
static int s_wait_reconnect_num = 0;

static char TZA[158][100] = {"AoE+12",
                             "NUT+11",
                             "SST+11",
                             "HST+10",
                             "HST10HDT,M3.2.0,M11.1.0",
                             "TAHT+10",
                             "AKST9AKDT,M3.2.0,M11.1.0",
                             "GAMT+9",
                             "EAST+8",
                             "PST+8",
                             "PST8PDT,M3.2.0,M11.1.0",
                             "MST+7",
                             "MST7MDT,M3.2.0,M11.1.0",
                             "MST7MDT,M4.1.0,M10.5.0",
                             "CST+6",
                             "CST6CDT,M3.2.0,M11.1.0",
                             "CST6CDT,M4.1.0,M10.5.0",
                             "EST+5",
                             "EST5EDT,M3.2.0,M11.1.0",
                             "PET+5",
                             "AST+4",
                             "AST4ADT,M3.2.0,M11.1.0",
                             "GYT+4",
                             "PYT4PYST,M10.1.0/0,M3.5.0/0",
                             "VET+4",
                             "AST-3",
                             "BRT+3",
                             "GFT+3",
                             "PMST3PMDT,M3.2.0,M11.1.0",
                             "UYT+3",
                             "WGT3WGST,M3.5.6/22,M10.5.6/23",
                             "GST+2",
                             "AZOT1AZOT,M3.5.0/0,M10.5.0/1",
                             "EGT1EGST,M3.5.0/0,M10.5.0/1",
                             "GMT+0",
                             "GMT+0BST-1,M3.5.0/1,M10.5.0/2",
                             "GMT+0IST-1,M3.5.0/1,M10.5.0/2",
                             "WET-0WEST-1,M3.5.0/1,M10.5.0/2",
                             "WET-0WEST-1,M5.2.0/2,M3.5.0/3",
                             "CET-1",
                             "CET-1CEST,M3.5.0/2,M10.5.0/3",
                             "WAT-1",
                             "CAT-2",
                             "EET-2",
                             "EET-2EEST,M3.5.0/0,M10.5.0/0",
                             "EET-2EEST,M3.5.0/2,M10.5.0/3",
                             "EET-2EEST,M3.5.0/3,M10.5.0/4",
                             "EET-2EEST,M3.5.5/0,M10.5.5/0",
                             "IST-2IDT,M3.4.4/26,M10.5.0",
                             "SAST-2",
                             "EAT-3",
                             "MSK-3",
                             "TRT-3",
                             "GET-4",
                             "GST-4",
                             "MUT-4",
                             "SAMT-4",
                             "SCT-4",
                             "AQTT-5",
                             "MVT-5",
                             "ORAT-5",
                             "PKT-5",
                             "TFT-5",
                             "TJT-5",
                             "UZT-5",
                             "YEKT-5",
                             "ALMT-6",
                             "KGT-6",
                             "OMST-6",
                             "HOVT-7",
                             "ICT-7",
                             "KRAT-7",
                             "NOVT-7",
                             "WIB-7",
                             "CHOT-8",
                             "CST-8",
                             "HKT-8",
                             "IRKT-8",
                             "MYT-8",
                             "PHST-8",
                             "SGT-8",
                             "UALT-8",
                             "WITA-8",
                             "JST-9",
                             "KST-9",
                             "PWT-9",
                             "WIT-9",
                             "YAKT-9",
                             "ChST-10",
                             "CHUT-10",
                             "PGT-10",
                             "SRET-10",
                             "VALT-10",
                             "BST-11",
                             "KOST-11",
                             "MAGT-11",
                             "NCT-11",
                             "NFT-11NFDT,M10.1.0/2,M4.1.0/3",
                             "PONT-11",
                             "SAKT-11",
                             "SBT-11",
                             "VUT-11",
                             "ANAT-12",
                             "CHAST-12:45CHADT,M9.5.0/02:45:00,M4.1.0/03:45:00",
                             "FJT-12FJST,M11.2.0/2,M1.2.0/3",
                             "GILT-12",
                             "MHT-12",
                             "NRT-12",
                             "NZST-12NZDT,M10.1.0/2,M3.3.0/3",
                             "PETT-12",
                             "TVT-12",
                             "WFT-12",
                             "PHOT-13",
                             "TKT-13",
                             "TOT-13",
                             "WST-13",
                             "LINT-14",
                             "IRDT-4:30",
                             "IST-5:30",
                             "NPT-5:45",
                             "MMT-6:30",
                             "AFT-4:30",
                             "ART+3",
                             "AMT-4",
                             "AEST-10AEDT,M10.1.0/2,M4.1.0/3",
                             "ACST-9:30ACDT,M10.1.0/2,M4.1.0/3",
                             "ACST-9:30",
                             "AEST-10",
                             "AWST-8",
                             "ACWST-8:45",
                             "AZT-4",
                             "BST-6",
                             "BTT-6",
                             "BOT+4",
                             "ACT+5",
                             "AMT+4",
                             "FNT+2",
                             "IOT-6",
                             "BNT-8",
                             "CVT+1",
                             "NST+3:30NDT,M3.2.0,M11.1.0",
                             "CLT4CLST,M9.1.0/0,M4.1.0/0",
                             "CCT-6:30",
                             "CXT-7",
                             "COT+5",
                             "TLT-9",
                             "ECT+5",
                             "FKST+3",
                             "EAST6EASST,M9.1.0/0,M4.1.0/0",
                             "CLST+3",
                             "CKT+10",
                             "GALT+6",
                             "RET-4",
                             "MART+9:30",
                             "VLAT-10",
                             "SRT+3",
                             "TMT-5",
                             "WAKT-12"};

uint8_t TZnumber = 34;
uint8_t modeClock = 0, changeWf = 0;
volatile bool isConnected = false, startCheckSTA = false, config_var = false, btnQr = true, firstTime = false, dfc = false, default_s = true, run_default = false, retry_connect = false, disconnect_show = false;
bool stateLed = false;
char dateStr[100];
char timeStr[100];
char country[100], firstDivision[100];

static void obtain_time(void);
static void initialize_sntp(void);
void wifi_init_sta(void);
bool readNVS();
bool writeNVS(uint8_t key);

void time_sync_notification_cb(struct timeval *tv)
{
    gpio_set_level(5, true);
    ESP_LOGI(TAG, "Notification of a time synchronization event");
    
}

#define PIN_SDA 10
#define PIN_SCL 8

const int QRCode_Version = 7;
const int QRCode_ECC = 0;

static const uint8_t connection_xbm[] = {
    0x00,
    0x00,
    0xE0,
    0x06,
    0xF8,
    0x1F,
    0x1E,
    0x38,
    0x07,
    0xE0,
    0xE1,
    0xC7,
    0x78,
    0x0F,
    0x1C,
    0x38,
    0x80,
    0x00,
    0xE0,
    0x07,
    0x20,
    0x06,
    0x00,
    0x00,
    0x80,
    0x00,
    0x80,
    0x01,
    0x00,
    0x01,
    0x00,
    0x00,
};
static const uint8_t unconnection_xbm[] = {
    0x03,
    0x00,
    0x82,
    0x01,
    0xEE,
    0x0F,
    0x0C,
    0x30,
    0x36,
    0x60,
    0x21,
    0x82,
    0xF0,
    0x1E,
    0x98,
    0x10,
    0x80,
    0x03,
    0xC0,
    0x03,
    0x60,
    0x04,
    0x00,
    0x0C,
    0x00,
    0x10,
    0x80,
    0x31,
    0x00,
    0xC0,
    0x00,
    0x80,
};

void twoDigit(int intValue, char _buffer[3])
{
    itoa(intValue, _buffer, 10);
    if (intValue < 10)
    {
        _buffer[1] = _buffer[0];
        _buffer[0] = '0';
    }
    _buffer[2] = '\0';
}

void showQRCode(u8g2_t screenVar, char *qrStr, int qrVer)
{
    enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_LOW; // Error correction level

    // // Make and print the QR Code symbol
    uint8_t qrcode[qrcodegen_BUFFER_LEN_FOR_VERSION(qrVer)];
    uint8_t tempBuffer[qrcodegen_BUFFER_LEN_FOR_VERSION(qrVer)];
    bool ok = qrcodegen_encodeText(qrStr, tempBuffer, qrcode, errCorLvl, qrVer, qrcodegen_VERSION_MAX, qrcodegen_Mask_AUTO, true);

    int size = qrcodegen_getSize(qrcode);
    // ESP_LOGI(TAG, "Size of QR Code: %d", size);
    uint8_t x0 = 1;
    uint8_t y0 = 18;
    uint8_t blockQr = x0 + size + 1;
    u8g2_ClearBuffer(&u8g2);
    for (uint8_t iy = y0 - 1; iy < y0 + size + 1; iy++)
    {
        for (uint8_t ix = x0 - 1; ix < x0 + size + 1; ix++)
        {
            u8g2_SetDrawColor(&screenVar, 1);
            u8g2_DrawPixel(&screenVar, ix, iy);
        }
    }

    u8g2_SetDrawColor(&screenVar, 1);
    u8g2_SetFont(&u8g2, u8g2_font_helvR14_tr);
    u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, "Internet Clock")) / 2, 15, "Internet Clock");
    u8g2_SetFont(&u8g2, u8g2_font_finderskeepers_tr);
    u8g2_DrawStr(&u8g2, ((SCREEN_W - blockQr - u8g2_GetStrWidth(&u8g2, "Please scan QR")) / 2) + blockQr, 24, "Please scan QR");
    u8g2_DrawStr(&u8g2, ((SCREEN_W - blockQr - u8g2_GetStrWidth(&u8g2, "or connect Wi-Fi")) / 2) + blockQr, 34, "or connect Wi-Fi");
    u8g2_DrawStr(&u8g2, ((SCREEN_W - blockQr - u8g2_GetStrWidth(&u8g2, ESP_AP_SSID)) / 2) + blockQr, 44, ESP_AP_SSID);
    u8g2_DrawStr(&u8g2, ((SCREEN_W - blockQr - u8g2_GetStrWidth(&u8g2, "with password")) / 2) + blockQr, 54, "with password");
    u8g2_DrawStr(&u8g2, ((SCREEN_W - blockQr - u8g2_GetStrWidth(&u8g2, ESP_AP_PASS)) / 2) + blockQr, 64, ESP_AP_PASS);

    if (ok)
    {
        for (uint8_t y = 0; y < size; y++)
        {
            for (uint8_t x = 0; x < size; x++)
            {
                if (qrcodegen_getModule(qrcode, x, y) == 0)
                { // change to == 1 to make QR code with black background
                    u8g2_SetDrawColor(&screenVar, 1);
                    u8g2_DrawPixel(&screenVar, x0 + x, y0 + y);

                    // Uncomment to make QR with 2H, 2W
                    // u8g2_DrawPixel(&u8g2, x0 + 2*x, y0 + 2*y);
                    // u8g2_DrawPixel(&u8g2, x0 + 2*x + 1, y0 + 2*y);
                    // u8g2_DrawPixel(&u8g2, x0 + 2*x, y0 + 2*y + 1);
                    // u8g2_DrawPixel(&u8g2, x0 + 2*x + 1, y0 + 2*y + 1);
                }
                else
                {
                    u8g2_SetDrawColor(&screenVar, 0);
                    u8g2_DrawPixel(&screenVar, x0 + x, y0 + y);
                    // u8g2_DrawPixel(&u8g2, x0 + 2*x, y0 + 2*y);
                    // u8g2_DrawPixel(&u8g2, x0 + 2*x + 1, y0 + 2*y);
                    // u8g2_DrawPixel(&u8g2, x0 + 2*x, y0 + 2*y + 1);
                    // u8g2_DrawPixel(&u8g2, x0 + 2*x + 1, y0 + 2*y + 1);
                }
            }
        }
        u8g2_SendBuffer(&screenVar);
    }
}

int button_task()
{
    gpio_pad_select_gpio(BTN_PIN);
    gpio_set_direction(BTN_PIN, GPIO_MODE_INPUT);
    gpio_set_pull_mode(BTN_PIN, GPIO_PULLUP_ONLY);

    return gpio_get_level(BTN_PIN);
}

void genQrCode()
{
    strcpy(WIFI_QR_CODE, "");
    strcat(WIFI_QR_CODE, HEADER_QR);
    strcat(WIFI_QR_CODE, ESP_AP_SSID);
    strcat(WIFI_QR_CODE, MIDDLE_QR);
    strcat(WIFI_QR_CODE, ESP_AP_PASS);
    strcat(WIFI_QR_CODE, END_QR);
}

wifi_config_t sta_config = {
    .sta = {
        .ssid = "",
        .password = "",
        .threshold.authmode = WIFI_AUTH_WPA2_PSK,
    },
};

wifi_config_t ap_config = {
    .ap = {
        .ssid = ESP_AP_SSID,
        .ssid_len = strlen(ESP_AP_SSID),
        .max_connection = ESP_AP_MAX_CONN,
        .authmode = WIFI_AUTH_WPA_WPA2_PSK},
};

static void passRandGen(char *strPass)
{
    int dec = esp_random();
    if (dec < 0)
    {
        dec = -dec;
    }
    while (dec % divideNum < (divideNum / 10))
    {
        dec *= 10;
    }
    dec %= divideNum;
    sprintf(strPass, "%d", dec);
    ESP_LOGI(TAG, "%s", strPass);
}

static void wifi_event_handler(void *arg, esp_event_base_t event_base,
                               int32_t event_id, void *event_data)
{
    if (event_id == WIFI_EVENT_AP_STACONNECTED)
    {
        wifi_event_ap_staconnected_t *event = (wifi_event_ap_staconnected_t *)event_data;
        ESP_LOGI(TAG, "station " MACSTR " join, AID=%d",
                 MAC2STR(event->mac), event->aid);
    }
    else if (event_id == WIFI_EVENT_AP_STADISCONNECTED)
    {
        wifi_event_ap_stadisconnected_t *event = (wifi_event_ap_stadisconnected_t *)event_data;
        ESP_LOGI(TAG, "station " MACSTR " leave, AID=%d",
                 MAC2STR(event->mac), event->aid);
        passRandGen(ESP_AP_PASS);
        strcpy((char *)ap_config.ap.password, ESP_AP_PASS);
        if (btnQr)
        {
            ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &ap_config));
            genQrCode();
            showQRCode(u8g2, WIFI_QR_CODE, QRCode_Version);
        }
    }
    else if (event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED && startCheckSTA == true)
    {
        if (s_retry_num < ESP_MAX_RETRY)
        {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        }
        else
        {
            ESP_LOGI(TAG, "Cannot connect to the AP");
            isConnected = false;
            if (firstTime)
            {
                u8g2_ClearBuffer(&u8g2);
                u8g2_SetDrawColor(&u8g2, 1);
                u8g2_SetFont(&u8g2, u8g2_font_helvR08_tr);
                u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, DISCONNECT_STR)) / 2, 26, DISCONNECT_STR);
                u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, NOTI1_STR)) / 2, 36, NOTI1_STR);
                u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, NOTI2_STR)) / 2, 46, NOTI2_STR);
                u8g2_SendBuffer(&u8g2);
                sys_delay_ms(2000);
                showQRCode(u8g2, WIFI_QR_CODE, QRCode_Version);
                retry_connect = true;
            }
            if (dfc)
            {
                u8g2_ClearBuffer(&u8g2);
                u8g2_SetDrawColor(&u8g2, 1);
                u8g2_SetFont(&u8g2, u8g2_font_helvR08_tr);
                u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, DISCONNECT_STR)) / 2, 31, DISCONNECT_STR);
                u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, "Run demo clock")) / 2, 41, "Run demo clock");
                u8g2_SendBuffer(&u8g2);
                sys_delay_ms(2000);
                dfc = false;
                run_default = true;
                modeClock = 1;
            }
        }
        ESP_LOGI(TAG, "connect to the AP fail");
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_CONNECTED && startCheckSTA == true)
    {
        ESP_LOGI(TAG, "Success connect to the AP");
        s_retry_num = 0;
        s_wait_reconnect_num = 0;
        config_var = true;
        isConnected = true;
        firstTime = false;
        run_default = false;
        retry_connect = false;
        modeClock = 2;
    }
}

static void wifi_init_softap(void)
{
    // Config DHCP to open pop-up on Android
    esp_netif_t *ap_netif = esp_netif_create_default_wifi_ap();
    esp_netif_ip_info_t ip_info;
    IP4_ADDR(&ip_info.ip, 124, 213, 16, 29);
    IP4_ADDR(&ip_info.gw, 124, 213, 16, 29);
    IP4_ADDR(&ip_info.netmask, 255, 0, 0, 0);
    esp_netif_dhcps_stop(ap_netif);
    esp_netif_set_ip_info(ap_netif, &ip_info);
    esp_netif_dhcps_start(ap_netif);

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &wifi_event_handler, NULL));

    strcpy((char *)ap_config.ap.password, ESP_AP_PASS);
    if (strlen(ESP_AP_PASS) == 0)
    {
        ap_config.ap.authmode = WIFI_AUTH_OPEN;
    }

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &ap_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    // esp_netif_ip_info_t ip_info;
    // esp_netif_get_ip_info(esp_netif_get_handle_from_ifkey("WIFI_AP_DEF"), &ip_info);

    char ip_addr[16];
    inet_ntoa_r(ip_info.ip.addr, ip_addr, 16);
    ESP_LOGI(TAG, "Set up softAP with IP: %s", ip_addr);

    ESP_LOGI(TAG, "wifi_init_softap finished. SSID:'%s' password:'%s'",
             ESP_AP_SSID, ESP_AP_PASS);
}

void wifi_init_sta(void)
{
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    if (strlen((char *)sta_config.sta.password) == 0)
    {
        sta_config.sta.threshold.authmode = WIFI_AUTH_OPEN;
    }
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &sta_config));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &ap_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    startCheckSTA = true;
    ESP_LOGI(TAG, "wifi_init_sta finished.");
}

void processStr(char string[], int size)
{
    int i, j;
    for (i = 0; i < size; i++)
    {
        if (string[i] == '+')
        {
            string[i] = ' ';
        }
        else if (string[i] == '%')
        {
            for (j = i; j < size; j++)
            {
                string[j] = string[j + 1];
            }
            for (j = i; j < size - 1; j++)
            {
                string[j] = string[j + 1];
            }
            switch (string[i])
            {
            case '7':
                string[i] = '\'';
                break;
            case '8':
                string[i] = '(';
                break;
            case '9':
                string[i] = ')';
                break;
            case 'C':
                string[i] = ',';
                break;
            default:
                break;
            }
        }
    }
}

void processStrSP(char string[], int size)
{
    int i, j;

    for (i = 0; i < size; i++)
    {
        int num = (int)string[i];
        if (num < 48 || (num > 57 && num < 65) || (num > 90 && num < 97) || num > 122)
        {
            while (i < size)
            {
                for (j = i; j < size; j++)
                {
                    string[j] = string[j + 1];
                }
                size--;
            }
        }
    }
}

static esp_err_t root_handler(httpd_req_t *req)
{
    char *sChar = strstr((const char *)root_start, "{S}");
    char *lChar = strstr((const char *)root_start, "{L}");
    char *rChar = strstr((const char *)root_start, "{R}");

    ESP_LOGI(TAG, "Serve root");
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send_chunk(req, root_start, sChar - root_start);
    if (strlen((char *)sta_config.sta.ssid) > 0)
    {
        httpd_resp_sendstr_chunk(req, (char *)sta_config.sta.ssid);
    }
    else
        httpd_resp_sendstr_chunk(req, "haven't setup");
    httpd_resp_send_chunk(req, sChar + 3, lChar - (sChar + 3));
    if (strlen((char *)country_firstDivision) > 0)
    {
        httpd_resp_sendstr_chunk(req, (char *)country_firstDivision);
    }
    else
        httpd_resp_sendstr_chunk(req, "haven't setup (UTC)");
    httpd_resp_send_chunk(req, lChar + 3, rChar - (lChar + 3));
    if (btnQr)
    {
        httpd_resp_sendstr_chunk(req, "Show clock");
    }
    else
    {
        httpd_resp_sendstr_chunk(req, "Show QR");
    }
    httpd_resp_send_chunk(req, rChar + 3, root_end - (rChar + 3));
    httpd_resp_send_chunk(req, NULL, 0);

    return ESP_OK;
}

static const httpd_uri_t root = {
    .uri = "/",
    .method = HTTP_GET,
    .handler = root_handler};

// Sacountryg config wifi
esp_err_t save_handler(httpd_req_t *req)
{
    uint8_t buffer[310];
    char ssid_r[32], password_r[64];
    httpd_req_recv(req, (char *)buffer, 310);
    // Process data of Configure Wi-Fi
    if (httpd_query_key_value((char *)buffer, "ssid", (char *)ssid_r, 32) == ESP_ERR_NOT_FOUND)
    {
        httpd_resp_set_status(req, "400");
        httpd_resp_send(req, "SSID required", -1);
        return ESP_OK;
    }
    if (httpd_query_key_value((char *)buffer, "password", (char *)password_r, 64) == ESP_ERR_NOT_FOUND)
    {
        httpd_resp_set_status(req, "400");
        httpd_resp_send(req, "Password is required", -1);
        return ESP_OK;
    }
    processStrSP(password_r, strlen((char *)password_r));
    ESP_LOGI(TAG, "SSID: %s, Pass: %s", ssid_r, password_r);
    if (strlen((char *)ssid_r) > 0)
    {
        ESP_LOGI(TAG, "Configure Wi-Fi");
        changeWf = 1;
        strcpy((char *)sta_config.sta.ssid, (const char *)ssid_r);
        strcpy((char *)sta_config.sta.password, (const char *)password_r);
        if (writeNVS(1) && writeNVS(2))
            ESP_LOGI(TAG, "Write done!");
    }

    char timezoneVar[10];
    if (httpd_query_key_value((char *)buffer, "timezone", (char *)timezoneVar, 10) == ESP_ERR_NOT_FOUND)
    {
        httpd_resp_set_status(req, "400");
        httpd_resp_send(req, "Timezone is required", -1);
        return ESP_OK;
    }
    if (httpd_query_key_value((char *)buffer, "country", (char *)country, 100) == ESP_ERR_NOT_FOUND)
    {
        httpd_resp_set_status(req, "400");
        httpd_resp_send(req, "Country is required", -1);
        return ESP_OK;
    }
    if (httpd_query_key_value((char *)buffer, "state", (char *)firstDivision, 100) == ESP_ERR_NOT_FOUND)
    {
        httpd_resp_set_status(req, "400");
        httpd_resp_send(req, "State is required", -1);
        return ESP_OK;
    }
    if (strcmp((const char *)country, "") != 0 && strcmp((const char *)firstDivision, "") != 0)
    {
        TZnumber = atoi((const char *)timezoneVar);
        processStr(country, strlen((const char *)country));
        processStr(firstDivision, strlen((const char *)firstDivision));
        ESP_LOGI(TAG, "Timezone: %d", TZnumber);
        ESP_LOGI(TAG, "Country: %s, First Division: %s", country, firstDivision);
        strcpy(country_firstDivision, "");
        strcat(country_firstDivision, country);
        strcat(country_firstDivision, ", ");
        strcat(country_firstDivision, firstDivision);
        if (writeNVS(0) && writeNVS(3))
            ESP_LOGI(TAG, "Write done!");
    }

    char *sChar = strstr((const char *)save_start, "{S}");

    httpd_resp_set_type(req, "text/html");
    httpd_resp_send_chunk(req, save_start, sChar - save_start);
    if (strlen((char *)ssid_r) > 0)
        httpd_resp_sendstr_chunk(req, "Your new Wi-Fi is saved!<br/>");
    if (strlen((char *)country) > 0)
    {
        if (strlen((char *)firstDivision) > 0)
            httpd_resp_sendstr_chunk(req, "Your new location is saved!<br/>");
        else
            httpd_resp_sendstr_chunk(req, "You need select both 'country' and 'first division' to setup new location<br/>");
    }
    if (strlen((char *)ssid_r) < 1 && strlen((char *)country) < 1)
    {
        httpd_resp_sendstr_chunk(req, "You haven't setuped any parameters.<br/>");
    }
    httpd_resp_send_chunk(req, sChar + 3, save_end - (sChar + 3));
    httpd_resp_send_chunk(req, NULL, 0);

    return ESP_OK;
}

static const httpd_uri_t save = {
    .uri = "/save",
    .method = HTTP_POST,
    .handler = save_handler};

// HTTP Error (404) Handler - Redirects all requests to the root page
esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err)
{
    // Set status
    httpd_resp_set_status(req, "302 Temporary Redirect");
    // Redirect to the "/" root directory
    httpd_resp_set_hdr(req, "Location", "/");
    // iOS requires content in the response to detect a captive portal, simply redirecting is not sufficient.
    httpd_resp_send(req, "Redirect to the captive portal", HTTPD_RESP_USE_STRLEN);

    ESP_LOGI(TAG, "Redirecting to root");
    return ESP_OK;
}

esp_err_t run_handler(httpd_req_t *req)
{
    char *sChar = strstr((const char *)root_start, "{S}");
    char *lChar = strstr((const char *)root_start, "{L}");
    char *rChar = strstr((const char *)root_start, "{R}");

    ESP_LOGI(TAG, "Serve run");
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send_chunk(req, root_start, sChar - root_start);
    if (modeClock == 0)
        modeClock = 1;
    if (strlen((char *)sta_config.sta.ssid) > 0)
    {
        httpd_resp_sendstr_chunk(req, (char *)sta_config.sta.ssid);
        if (modeClock == 1)
        {
            run_default = false;
            btnQr = false;
            u8g2_ClearBuffer(&u8g2);
            u8g2_SetDrawColor(&u8g2, 1);
            u8g2_SetFont(&u8g2, u8g2_font_helvR08_tr);
            u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, CONNECT_STR)) / 2, 36, CONNECT_STR);
            u8g2_SendBuffer(&u8g2);
            dfc = true;
        }
    }
    else
        httpd_resp_sendstr_chunk(req, "haven't setup");
    httpd_resp_send_chunk(req, sChar + 3, lChar - (sChar + 3));
    if (strlen((char *)country_firstDivision) > 0)
    {
        httpd_resp_sendstr_chunk(req, (char *)country_firstDivision);
    }
    else
        httpd_resp_sendstr_chunk(req, "haven't setup (UTC)");
    httpd_resp_send_chunk(req, lChar + 3, rChar - (lChar + 3));
    if (btnQr)
    {
        httpd_resp_sendstr_chunk(req, "Show QR");
    }
    else
    {
        httpd_resp_sendstr_chunk(req, "Show clock");
    }
    httpd_resp_send_chunk(req, rChar + 3, root_end - (rChar + 3));
    httpd_resp_send_chunk(req, NULL, 0);

    if (!dfc)
    {
        if (btnQr)
        {
            if (strlen((char *)sta_config.sta.ssid) > 0)
            {
                modeClock = 2;
                config_var = true;
                ESP_LOGI(TAG, "Run internet clock");
            }
            else
            {
                modeClock = 1;
                run_default = true;
                ESP_LOGI(TAG, "Run demo clock");
            }
            btnQr = false;
        }
        else
        {
            run_default = false;
            config_var = false;
            genQrCode();
            showQRCode(u8g2, WIFI_QR_CODE, QRCode_Version);
            btnQr = true;
        }
    }
    if (changeWf == 1)
        changeWf = 2;

    return ESP_OK;
}

static const httpd_uri_t run = {
    .uri = "/run",
    .method = HTTP_POST,
    .handler = run_handler};

static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.max_open_sockets = 7;
    config.lru_purge_enable = true;

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK)
    {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &root);
        httpd_register_uri_handler(server, &save);
        httpd_register_uri_handler(server, &run);
        httpd_register_err_handler(server, HTTPD_404_NOT_FOUND, http_404_error_handler);
    }
    return server;
}

bool readNVS()
{
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &my_handle);
    if (err != ESP_OK)
    {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
        return false;
    }
    else
    {
        printf("Done!\n");

        size_t str1Size = 32;
        err = nvs_get_str(my_handle, "SSID", (char *)sta_config.sta.ssid, &str1Size);
        switch (err)
        {
        case ESP_OK:
            printf("Done\n");
            printf("SSID = %s\n", sta_config.sta.ssid);
            break;
        default:
            return false;
        }

        size_t str2Size = 64;
        err = nvs_get_str(my_handle, "PWD", (char *)sta_config.sta.password, &str2Size);
        switch (err)
        {
        case ESP_OK:
            printf("Done\n");
            printf("Password = %s\n", sta_config.sta.password);
            break;
        default:
            return false;
        }

        err = nvs_get_u8(my_handle, "TZnumber", &TZnumber);
        switch (err)
        {
        case ESP_OK:
            printf("Done\n");
            printf("TZnumber = %d\n", TZnumber);
            break;
        default:
            break;
        }
        size_t str3Size = 200;
        err = nvs_get_str(my_handle, "country", (char *)country_firstDivision, &str3Size);
        switch (err)
        {
        case ESP_OK:
            printf("Done\n");
            printf("Country & First Division = %s\n", country_firstDivision);
            break;
        default:
            break;
        }
    }
    nvs_close(my_handle);
    return true;
}

bool writeNVS(uint8_t key)
{
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &my_handle);

    if (err != ESP_OK)
    {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    }
    else
    {
        switch (key)
        {
        case 0:
            err = nvs_set_u8(my_handle, "TZnumber", TZnumber);
            break;
        case 1:
            err = nvs_set_str(my_handle, "SSID", (char *)sta_config.sta.ssid);
            break;
        case 2:
            err = nvs_set_str(my_handle, "PWD", (char *)sta_config.sta.password);
            break;
        case 3:
            err = nvs_set_str(my_handle, "country", (char *)country_firstDivision);
            break;
        default:
            break;
        }
        if (err != ESP_OK)
            return false;
        else
        {
            err = nvs_commit(my_handle);

            if (err != ESP_OK)
                return false;
        }
    }
    nvs_close(my_handle);
    return true;
}

void showOLED(struct tm _timeinfo, char *strFooter, int _sec)
{
    char wDay_str[7][6] = {"Sun, ", "Mon, ", "Tue, ", "Wed, ", "Thu, ", "Fri, ", "Sat, "};

    strcpy(dateStr, "");
    char buffer[3];
    strcat(dateStr, wDay_str[_timeinfo.tm_wday]);
    twoDigit(_timeinfo.tm_mday, buffer);
    strcat(dateStr, buffer);
    strcat(dateStr, " - ");
    twoDigit(_timeinfo.tm_mon + 1, buffer);
    strcat(dateStr, buffer);
    strcat(dateStr, " - ");
    char timeBuf[5];
    itoa(_timeinfo.tm_year + 1900, timeBuf, 10);
    timeBuf[4] = '\0';
    strcat(dateStr, timeBuf);
    ESP_LOGI(TAG, "Date: %s", dateStr);

    // Process timeStr with some parameters as Hours, Minutes, Seconds
    strcpy(timeStr, "");
    twoDigit(_timeinfo.tm_hour, buffer);
    strcat(timeStr, buffer);
    strcat(timeStr, ":");
    twoDigit(_timeinfo.tm_min, buffer);
    strcat(timeStr, buffer);
    twoDigit(_sec, buffer);
    ESP_LOGI(TAG, "Time: %s", timeStr);

    u8g2_ClearBuffer(&u8g2);
    u8g2_SetDrawColor(&u8g2, 1);
    ESP_LOGI(TAG, "u8g2_SetFont");
    u8g2_SetFont(&u8g2, u8g2_font_helvR08_tr);
    ESP_LOGI(TAG, "u8g2_DrawStr");
    u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, dateStr) - 16) / 2, 12, dateStr);

    u8g2_DrawStr(&u8g2, 1, 62, strFooter);
    // Calculate display location
    ESP_LOGI(TAG, "u8g2_SetFont");
    u8g2_SetFont(&u8g2, u8g2_font_logisoso30_tn);
    uint8_t sizeTimeStr = u8g2_GetStrWidth(&u8g2, timeStr);
    u8g2_SetFont(&u8g2, u8g2_font_logisoso16_tn);
    uint8_t sizeSec = u8g2_GetStrWidth(&u8g2, buffer);
    u8g2_SetFont(&u8g2, u8g2_font_logisoso30_tn);
    u8g2_DrawStr(&u8g2, (SCREEN_W - sizeTimeStr - sizeSec - 1) / 2, 50, timeStr);
    u8g2_SetFont(&u8g2, u8g2_font_logisoso16_tn);
    u8g2_DrawStr(&u8g2, (SCREEN_W - sizeTimeStr - sizeSec - 1) / 2 + sizeTimeStr + 1, 50, buffer);

    if (isConnected)
    {
        u8g2_DrawXBM(&u8g2, 112, 0, 16, 16, connection_xbm);
    }
    else
    {
        u8g2_DrawXBM(&u8g2, 112, 0, 16, 16, unconnection_xbm);
    }
    ESP_LOGI(TAG, "u8g2_SendBuffer");
    u8g2_SendBuffer(&u8g2);
}

void connection_state_task(void *pvParameters)
{
    const TickType_t tDelay = 1000 / portTICK_PERIOD_MS;

    while (1)
    {

        stateLed = !stateLed;
        if (isConnected)
        {
            gpio_set_level(4, stateLed);
            gpio_set_level(3, false);
            gpio_set_level(5, false);
        }
        else if (!isConnected && modeClock != 0)
        {
            gpio_set_level(3, stateLed);
            gpio_set_level(4, false);
            gpio_set_level(5, false);
        }
        vTaskDelay(tDelay);
    }
    vTaskDelete(NULL);
}

void reconnect_task(void *pvParameters)
{
    const TickType_t tDelay = 60000 / portTICK_PERIOD_MS;
    while (1)
    {
        if (modeClock == 2 && !btnQr)
        {
            esp_wifi_disconnect();
            esp_wifi_connect();
            if (!isConnected)
            {
                s_wait_reconnect_num++;
                disconnect_show = true;
            }
        }
        if (firstTime && !isConnected && retry_connect && modeClock == 0)
        {
            s_retry_num = 0;
            esp_wifi_disconnect();
            esp_wifi_connect();
            ESP_LOGI(TAG, "Connecting to previous Wi-Fi");
        }
        vTaskDelay(tDelay);
    }
    vTaskDelete(NULL);
}

void app_main(void)
{
    gpio_reset_pin(3);
    gpio_set_direction(3, GPIO_MODE_OUTPUT);
    gpio_reset_pin(4);
    gpio_set_direction(4, GPIO_MODE_OUTPUT);
    gpio_reset_pin(5);
    gpio_set_direction(5, GPIO_MODE_OUTPUT);

    passRandGen(ESP_AP_PASS);
    esp_log_level_set("httpd_uri", ESP_LOG_ERROR);
    esp_log_level_set("httpd_txrx", ESP_LOG_ERROR);
    esp_log_level_set("httpd_parse", ESP_LOG_ERROR);

    genQrCode();

    u8g2_esp32_hal_t u8g2_esp32_hal = U8G2_ESP32_HAL_DEFAULT;
    u8g2_esp32_hal.sda = PIN_SDA;
    u8g2_esp32_hal.scl = PIN_SCL;
    u8g2_esp32_hal_init(u8g2_esp32_hal);

    // a structure which will contain all the data for one display
    u8g2_Setup_ssd1306_i2c_128x64_noname_f(
        &u8g2,
        U8G2_R0,
        u8g2_esp32_i2c_byte_cb,
        u8g2_esp32_gpio_and_delay_cb); // init u8g2 structure
    u8x8_SetI2CAddress(&u8g2.u8x8, 0x78);

    ESP_LOGI(TAG, "u8g2_InitDisplay");
    u8g2_InitDisplay(&u8g2); // send init sequence to the display, display is in sleep mode after this,

    ESP_LOGI(TAG, "u8g2_SetPowerSave");
    u8g2_SetPowerSave(&u8g2, 0); // wake up display
    ESP_LOGI(TAG, "u8g2_ClearBuffer");
    u8g2_ClearBuffer(&u8g2);

    ESP_LOGI(TAG, "WIFI_QR_CODE: %s", WIFI_QR_CODE);

    showQRCode(u8g2, WIFI_QR_CODE, QRCode_Version);
    // u8g2_DrawXBM(&u8g2,0, 0, 117, 60, logo_xbm);
    // u8g2_SendBuffer(&u8g2);

    ESP_ERROR_CHECK(esp_netif_init());

    // Create default event loop needed by the  main app
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    // Initialize NVS needed by Wi-Fi
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);

    // Initialize Wi-Fi including netif with default config

    esp_netif_create_default_wifi_sta();

    // Initialise ESP32 in SoftAP mode
    wifi_init_softap();

    // Start the server for the first time
    start_webserver();

    // Start the DNS server that will redirect all queries to the softAP IP
    start_dns_server();

    xTaskCreate(connection_state_task, "cnn_state", 4096, NULL, 5, NULL);

    xTaskCreate(reconnect_task, "reconnect_task", 4096, NULL, 5, NULL);

    sys_delay_ms(10000);

    if (readNVS())
    {
        u8g2_ClearBuffer(&u8g2);
        u8g2_SetDrawColor(&u8g2, 1);
        u8g2_SetFont(&u8g2, u8g2_font_helvR08_tr);
        u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, "Loading the clock")) / 2, 31, "Loading the clock");
        u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, "Please wait...")) / 2, 41, "Please wait...");
        u8g2_SendBuffer(&u8g2);
        firstTime = true;
        if (writeNVS(0) && writeNVS(1) && writeNVS(2) && writeNVS(3))
            ESP_LOGI(TAG, "Saved parameters again!");
        ESP_ERROR_CHECK(esp_wifi_stop());
        wifi_init_sta();
    }
    else
    {
        u8g2_ClearBuffer(&u8g2);
        u8g2_SetDrawColor(&u8g2, 1);
        u8g2_SetFont(&u8g2, u8g2_font_helvR08_tr);
        u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, "Loading demo clock...")) / 2, 36, "Loading demo clock...");
        u8g2_SendBuffer(&u8g2);
        run_default = true;
        modeClock = 1;
        sys_delay_ms(2000);
    }

    time_t now, df_now;
    struct tm timeinfo, df_timeinfo;
    int prev_seconds = -1, df_prev_sec = -1, counter = 0;

    while (1)
    {
        // Default Clock when haven't set anything
        if (run_default && modeClock == 1)
        {
            btnQr = false;
            s_retry_num = 0;
            s_wait_reconnect_num = 0;
            if (default_s == true)
            {
                struct tm tm;

                tm.tm_year = 2010 - 1900;

                tm.tm_mon = 8;

                tm.tm_mday = 11;

                tm.tm_hour = 0;

                tm.tm_min = 0;

                tm.tm_sec = 0;

                time_t t = mktime(&tm);

                struct timeval now_timeval = {.tv_sec = t};

                settimeofday(&now_timeval, NULL);
                default_s = false;
            }
            time(&df_now);
            // Set timezone to China Standard Time
            setenv("TZ", TZA[TZnumber], 1);
            tzset();
            localtime_r(&df_now, &df_timeinfo);
            if (df_timeinfo.tm_sec != df_prev_sec)
            {
                df_prev_sec = df_timeinfo.tm_sec;
                showOLED(df_timeinfo, "Demo Clock", df_prev_sec);
                counter++;
            }
            if (counter == 60)
            {
                btnQr = true;
                counter = 0;
                u8g2_ClearBuffer(&u8g2);
                u8g2_SetDrawColor(&u8g2, 1);
                u8g2_SetFont(&u8g2, u8g2_font_helvR08_tr);
                u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, "Please setup new Wi-Fi")) / 2, 36, "Please setup new Wi-Fi");
                u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, "to update exactly time")) / 2, 46, "to update exactly time");
                u8g2_SendBuffer(&u8g2);
                sys_delay_ms(2000);
                genQrCode();
                showQRCode(u8g2, WIFI_QR_CODE, QRCode_Version);
                sys_delay_ms(10000);
            }
            while (!button_task())
            {
                run_default = false;
                // ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &ap_config));
                genQrCode();
                showQRCode(u8g2, WIFI_QR_CODE, QRCode_Version);
                btnQr = true;
                sys_delay_ms(500);
            }
        }
        if (changeWf == 2 && !btnQr && modeClock != 0)
        {
            ESP_ERROR_CHECK(esp_wifi_stop());
            wifi_init_sta();
            changeWf = 0;
        }
        if (config_var && changeWf == 0 && modeClock == 2)
        {
            btnQr = false;
            time(&now);
            localtime_r(&now, &timeinfo);
            // Is time set? If not, tm_year will be (1970 - 1900).
            if (timeinfo.tm_year < (2016 - 1900))
            {
                obtain_time();
                // update 'now' variable with current time
                time(&now);
            }

            // Set timezone
            setenv("TZ", TZA[TZnumber], 1);
            tzset();
            localtime_r(&now, &timeinfo);
            if (timeinfo.tm_year >= (2016 - 1900) && timeinfo.tm_sec != prev_seconds)
            {
                prev_seconds = timeinfo.tm_sec;
                showOLED(timeinfo, country_firstDivision, prev_seconds);
                if (timeinfo.tm_hour == 1 && timeinfo.tm_min == 0 && timeinfo.tm_sec == 0)
                {
                    if (isConnected) esp_restart();
                    
                }
            }
            if (!isConnected && s_wait_reconnect_num % DISCONNECT_SHOW_QR_FREQ == 0 && disconnect_show)
            {
                btnQr = true;
                char num_buf[4];
                itoa(s_wait_reconnect_num, num_buf, 10);
                strcpy(STR_TIME_LOST, "");
                strcat(STR_TIME_LOST, "Lost internet ");
                strcat(STR_TIME_LOST, num_buf);
                strcat(STR_TIME_LOST, "'");
                ESP_LOGI(TAG, "Merge done!");
                u8g2_ClearBuffer(&u8g2);
                u8g2_SetDrawColor(&u8g2, 1);
                u8g2_SetFont(&u8g2, u8g2_font_helvR08_tr);
                u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, STR_TIME_LOST)) / 2, 24, STR_TIME_LOST);
                u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, "Time may not be exact")) / 2, 34, "Time may not be exact");
                u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, "Please check internet")) / 2, 46, "Please check internet");
                u8g2_DrawStr(&u8g2, (SCREEN_W - u8g2_GetStrWidth(&u8g2, "or setup new Wi-Fi")) / 2, 56, "or setup new Wi-Fi");
                u8g2_SendBuffer(&u8g2);
                sys_delay_ms(5000);
                genQrCode();
                showQRCode(u8g2, WIFI_QR_CODE, QRCode_Version);
                sys_delay_ms(10000);
                disconnect_show = false;
            }
            while (!button_task())
            {
                config_var = false;
                // ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &ap_config));
                genQrCode();
                showQRCode(u8g2, WIFI_QR_CODE, QRCode_Version);
                btnQr = true;
                sys_delay_ms(500);
            }
        }

        if (btnQr && !run_default && !config_var && modeClock != 0 && changeWf == 0)
        {
            while (!button_task())
            {
                if (strlen((char *)sta_config.sta.ssid) == 0)
                {
                    run_default = true;
                }
                else
                {
                    config_var = true;
                }
                btnQr = false;
            }
        }
    }
}

static void obtain_time(void)
{
    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */

    initialize_sntp();

    // wait for time to be set
    time_t now = 0;
    struct tm timeinfo = {0};
    int retry = 0;
    const int retry_count = 10;
    while (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET && ++retry < retry_count)
    {
        ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    time(&now);
    localtime_r(&now, &timeinfo);
}

static void initialize_sntp(void)
{
    ESP_LOGI(TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_set_time_sync_notification_cb(time_sync_notification_cb);
    sntp_init();
}
